#include "le.h"
#include <stdlib.h>

/**
  *typedef struct elem{
  *int val;
  *
  *struct elem * next;
  *} elem;
  **
  *struct llist{
  *  elem * head;
  *  int tam;
  *};
*/

struct llist * create_l(){
  struct llist *head;
  head = malloc(sizeof(struct llist));

  if(head==0){
    return head;
  }

  head->tam  = 0;
  head->head = 0;

  return head;
}

elem * create_node(int val){
  elem *node;
  node = malloc(sizeof(elem));

  node->val = val;
  node->next = 0;

  return node;
}

int insert_l(struct llist *desc, elem * prev, elem * item){
  if(item == 0){
    return 0;
  }else if(prev == 0){
    item->next = desc->head;
    desc->head = item;
    desc->tam++;
    return 1;
  }else{
    item->next = prev->next;
    prev->next = item;
    desc->tam++;
    return 1;
  }
}

int delete_l(struct llist *desc, elem * prev){
  elem *aux;
  if(desc->tam==0){
    return 0;
  }else if(prev==0){
    desc->head = 0;
    desc->tam = 0;
    return 1;
  }else{
    aux = prev->next->next;
    free(prev->next);
    prev->next = aux;
    desc->tam--;
    return 1;
  }
}

elem * get_l(struct llist *desc, int pos){
  if(pos>desc->tam){
    return 0;
  }
  elem *aux = desc->head;
  int i = 1;
  while(aux->next != 0 && i != pos){
    aux = aux->next;
  }
  return aux;
}

int set_l(struct llist *desc, int pos, int val){
  elem *node = get_l(desc,pos);

  if(node == 0){
    return 0;
  }
  node->val = val;

  return 1;
}

elem * locate_l(struct llist *desc, elem * prev, int val){
  elem *node;

  if(desc->head == 0)
    return 0;
    if(prev == 0){
      node = desc->head;
    }else
      node = prev;
  while(node->next != 0){
    if(node->val == val){
      return node;
    }
    node = node->next;
  }
  if(node->val == val){
    return node;
  }
  return node->next;
}

int length_l(struct llist *desc){
  return desc->tam;
}

void destroy_l(struct llist *desc){
  while(desc->head != 0){
    delete_l(desc,0);
  }
}
