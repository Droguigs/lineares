#include <stdlib.h>
#include "ls.h"

/**
  *struct list{
  *int tam;
  *int max;
  *elem * arm;
  *};
  */

struct list * create(int max){
  if(max<1)
    return 0;
  struct list *head;
  head = (struct list*)malloc(sizeof(struct list));

  head->tam = 0;
  head->max = max;

  head->arm = malloc(sizeof(elem)*max);

  return head;
}

int insert(struct list *desc, int pos, elem item){
  if(desc->tam >= desc->max){
    return 0;
  }else if(pos-1 > desc->tam || pos < 1){
    return 0;
  }else{
    for(int i = (desc->tam)-1; i>=pos-1; i--){
      desc->arm[i+1] = desc->arm[i];
    }
    desc->arm[pos-1] = item;
    desc->tam++;
    return 1;
  }
}

int delete(struct list *desc, int pos){
  if(pos > desc->tam){
    return 0;
  }else{
    desc->tam-=1;
    for(int i=pos;i<desc->tam;i++){
      desc->arm[i] = desc->arm[i+1];
    }
    return 1;
  }
}

elem get(struct list *desc, int pos){
  if(pos > desc->tam){
    return 0;
  }else
    return desc->arm[pos-1];
}

int set(struct list *desc, int pos, elem item){
  if(pos > desc->tam || pos < 1){
    return 0;
  }else{
    desc->arm[pos-1] = item;
    return 1;
  }
}

int locate(struct list *desc, int pos, elem item){
  for(int i=pos-1; i < desc->tam; i++){
    if(desc->arm[i] == item){
      return i+1;
    }
  }
  return 0;
}

int length(struct list *desc){
  return desc->tam;
}

int max(struct list *desc){
  return desc->max;
}

int full(struct list *desc){
  if(length(desc)==max(desc)){
    return 1;
  }
  return 0;
}

void destroy(struct list *desc){
  free(desc->arm);
  free(desc);
}
