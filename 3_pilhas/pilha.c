#include "pilha.h"
#include <stdlib.h>

#define MAX 5

/** Descritor da pilha
  *
  *struct pilha{
  *struct llist *l;
  *};
  */

struct pilha * create(){
  struct pilha *p;
  p = malloc(sizeof(struct pilha));
  p->l = create_l();

  return p;
}

int makenull(struct pilha * p){
  if(vazia(p)){
    destroy_l(p->l);
    return 1;
  }
  return 0;
}

int top(struct pilha * p){
  return (get_l(p->l,0))->val;
}

int pop(struct pilha * p){
  return delete_l(p->l,0);
}

int push(struct pilha * p, int val){
  struct elem *item;
  item = create_node(val);
  return insert_l(p->l,0,item);
}

 int vazia(struct pilha *p){
   if (p->l->head == NULL)
     return 1;
   return 0;
}

void destroy(struct pilha * p){
  makenull(p);
  free (p);
}
